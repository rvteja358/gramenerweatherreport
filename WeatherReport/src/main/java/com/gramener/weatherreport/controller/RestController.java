package com.gramener.weatherreport.controller;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import com.gramener.weatherreport.service.ConsumeWebService;

/*
 * this class used for WetherReport Controller
 */
@org.springframework.web.bind.annotation.RestController
public class RestController {

	@Autowired
	ConsumeWebService consumeWebService;
	
	/*
	 * this method used for WetherReporting
	 */
	@GetMapping("/current")
	public ResponseEntity<String> weatherReport(@RequestHeader("location") String location) {

		JSONObject weatherReport = consumeWebService.getData(location);

		String result = weatherReport.toString();

		return ResponseEntity.ok(result);
	}

}
